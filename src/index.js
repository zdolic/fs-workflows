import React from 'react';
import ReactDOM from 'react-dom';
import "./main.css";
import "./assets/animate.css";

import "./assets/fontAwesome/css/font-awesome.min.css";

import Routes from './router';
import * as serviceWorker from './serviceWorker';

import baobabTree from './data/state';
import { root } from 'baobab-react/higher-order';

const RootedApp = root(baobabTree, Routes);

ReactDOM.render(<RootedApp />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
