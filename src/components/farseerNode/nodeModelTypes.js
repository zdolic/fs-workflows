
export default {
	startNode:{
		key:"startNode",
		name: "Start node",
		desc:"Driver, assumption or strategic input", 
		backgroundColor:"rgb(0, 192, 255)", //"#119DA4",
		icon:"fa fa-flag-checkered",
		percentage:100,
		confirmed:false,
		hasDueDate:false,
		inputs:[],
		outputs:["START"]
		//allowedInputs:false
	},
	dataCollectorNode:{
		key:"dataCollectorNode",
		name: "Data collect node",
		desc:"Call for data input on clusters or flows", 
		backgroundColor:"#16DB93",
		icon:"fa fa-filter",
		percentage:0,
		confirmed:false,
		hasDueDate:true,
		inputs:["DATA IN"],
		outputs:["DATA OUT"]
		//allowedInputs:false
	},
	checkPointNode:{
		key:"checkPointNode",
		name: "Check point",
		desc:"Send notification and request for approval to continiue", 
		backgroundColor:"#FF9505",
		icon:"fa fa-hourglass-2",
		percentage:0,
		confirmed:false,
		hasDueDate:true,
		inputs:["INPUTS"],
		outputs:["ACCEPT", "REJECT"]
		//allowedInputs:false
	},
	endNode:{
		key:"endNode",
		name: "End process",
		desc:"Finish process point", 
		backgroundColor:"#F8333C", 
		icon:"fa fa-handshake-o",
		percentage:0,
		confirmed:false,
		hasDueDate:true,
		inputs:["FINISH"],
		outputs:[]
		//allowedInputs:false
	},

}