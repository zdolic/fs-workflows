import { DefaultPortModel, NodeModel } from '@projectstorm/react-diagrams';


export class JSCustomNodeModel extends NodeModel {
	constructor(options = {}, fsData) {
		super({
			...options,
			type: 'farseer-node'
		});
		this.fsData = fsData;
		this.portsOut = [];
		this.portsIn = [];
	}


	removePort(port){
		super.removePort(port);
		if (port.getOptions().in) {
			this.portsIn.splice(this.portsIn.indexOf(port));
		} else {
			this.portsOut.splice(this.portsOut.indexOf(port));
		}
	}

	addPort(port){
		super.addPort(port);
		if (port.getOptions().in) {
			if (this.portsIn.indexOf(port) === -1) {
				this.portsIn.push(port);
			}
		} else {
			if (this.portsOut.indexOf(port) === -1) {
				this.portsOut.push(port);
			}
		}
		return port;
	}

	addInPort(name){
		return this.addPort(
			new DefaultPortModel({
				in: true,
				name:name
			})
		);
	}
	addOutPort(name){
		return this.addPort(
			new DefaultPortModel({
				in: false,
				name:name
			})
		);
	}

	checkInPort(name){
		return this.portsIn.includes(name);
	}

	checkOutPort(name){
		return this.portsOut.includes(name);
	}


	serialize() {
		return {
			...super.serialize(),
			fsData: this.fsData
		};
	}

	deserialize(event, engine) {
		//console.log("DESERIALIZING", ob);
		super.deserialize(event, engine);
		this.fsData = event.data.fsData;
	}

	getInPorts() {
		return this.portsIn;
	}

	getOutPorts() {
		return this.portsOut;
	}
	
}
