import * as React from 'react';
import { PortWidget } from '@projectstorm/react-diagrams';
import _ from "lodash"
import moment from "moment"
import pubsub from "../../data/pubsub"

export class JSCustomNodeWidget extends React.Component {
	render() {
		//console.log(this.props.node);
		let inputs = _.filter(this.props.node.ports, p=>{
			return p.options.in;
		});

		let outputs =  _.filter(this.props.node.ports, p=>{
			return !p.options.in;
		});

		

		let status = "";

		if(this.props.node.fsData.key==="dataCollectorNode" || this.props.node.fsData.key==="checkPointNode"){
			status = this.props.node.fsData.percentage>=100 ? <h5 className="p-1 m-0"><i className="fa fa-check p-1"/> - OK</h5> : null;;
			if(status===null){
				status = <h6 className="p-1 m-0">{this.props.node.fsData.percentage } % - {moment(this.props.node.fsData.dueDate).format("ll")}</h6>
			}
		}
		

		return (

			<div key={this.props.node.options.id} onDoubleClick={()=>{
				pubsub.emit("nodeDoubleClick", this.props.node);
			}} className="my-node" style={{ backgroundColor: this.props.node.fsData.backgroundColor}} >


				<div className="header p-1"><b>  {this.props.node.fsData.name}</b></div>

				{status}
				<div className="p-1">{this.props.node.fsData.desc}</div>


				
			
				<div className="body">

					<div className="inputs">

						{
						_.map(inputs, (port, key)=>{
								return (
									<div key={port.options.name}  className="port-item">
									<PortWidget engine={this.props.engine} port={port}>
										<div className="port-circle" />
									</PortWidget>
									<div className="port-name">{port.options.name}</div>
								</div>)
							
						})
						}
					</div>
					<div className="outputs">
					{
						_.map(outputs, (port, key)=>{
								return (
									<div key={port.options.name}  className="port-item">
									<div className="port-name">{port.options.name}</div>
									<PortWidget engine={this.props.engine} port={port}>
										<div className="port-circle" />
									</PortWidget>
									
								</div>)
							
						})
					}
					</div>
				</div>

			</div>
		);
	}
}

