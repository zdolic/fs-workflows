import * as React from 'react';
import { JSCustomNodeModel } from './NodeModel';
import { JSCustomNodeWidget } from './NodeWidget';
import { AbstractReactFactory } from '@projectstorm/react-canvas-core';

export class JSCustomNodeFactory extends AbstractReactFactory {
	constructor() {
		super('farseer-node');
	}

	generateModel(event) {
		return new JSCustomNodeModel( );
	}

	generateReactWidget(event) {
		return <JSCustomNodeWidget engine={this.engine} node={event.model} />;
	}
}

