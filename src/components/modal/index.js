import React, { Component } from 'react';
import { branch } from 'baobab-react/higher-order';

//import _ from "lodash"
//<div className="modal-content modal-dialog-centered">

class MainModal extends Component {

	render(){
		return <div className="modal fade " id="mainModal" tabIndex="-1"  role="dialog" aria-hidden="true">
		    <div className={"modal-dialog "+this.props.data.width}>
		      <div className="modal-content">

		      	{
		      		this.props.data.header ?  <div className="modal-header">
			          <h5 className="modal-title">{this.props.data.header}</h5>
			          <button type="button" className="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				       </button>
			        </div> 
			        : null
			    }
		       

		        <div className="modal-body" style={this.props.data.bodyStyle}>
		          {this.props.data.content}
		        </div>
		        
		        { this.props.data.showBottomCloseButton ? <div className="modal-footer"><button type="button" className="btn btn-secondary" data-dismiss="modal">CLOSE</button> </div> : null }
		       
		      </div>
		      
		    </div>
		  </div>
	}
}


export default branch({
	data: ["modalData"]
}, MainModal);


