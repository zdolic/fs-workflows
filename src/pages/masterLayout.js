import React from 'react';
import SideToolBox from "./sideTray"
import Modal from "../components/modal"

var App = (props)=>{
  return (
    < >
     <Modal/>
     <div className="container-fluid">
    	<div className="row">
      	<div className="left-side  col-md-2">
          <SideToolBox/>
        </div>

        <div className="right-side col-md-10">
          {props.children}
        </div>
      </div>
    </div>


    </>
  );
}

export default App;
