import React, { Component } from 'react';
import _ from "lodash"


import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';


import {
  formatDate,
  parseDate,
} from 'react-day-picker/moment';


class NodeModalForm extends Component{
	constructor(props){
		super();
		//this._input = null;
		let isNew = props.newNodeReady!==undefined;

		this.state = {
			name: props.data.name,
			desc: isNew ? "" : props.data.desc,
			percentage: props.data.percentage,
			dueDate: new Date(),
			inputs:props.data.inputs.join(";"),
			outputs:props.data.outputs.join(";")
		}
	}
	componentDidUpdate(prevProps){
		let isNew = this.props.newNodeReady!==undefined && prevProps.data.name!==this.props.data.name;



		if(prevProps.id!==this.props.id || isNew){
			this.setState({
				name: this.props.data.name,
				desc: isNew ? "" : this.props.data.desc,
				percentage: this.props.data.percentage,
				dueDate: this.props.data.dueDate,
				inputs:this.props.data.inputs.join(";"),
				outputs:this.props.data.outputs.join(";")
			});
		}
	}
	handlePortsIO(e){
		this.setState({[e.target.name]:e.target.value});
	}
	submit(){
		let node = _.clone(this.props.data)
		node.name = this.state.name;
		node.dueDate = this.state.dueDate
		node.percentage = Number(this.state.percentage)
		node.desc = this.state.desc;

		node.inputs = _.filter(this.state.inputs.split(";"), v=>v!=="")
		node.outputs = _.filter(this.state.outputs.split(";"), v=>v!=="")

		if(this.props.newNodeReady){
			this.props.newNodeReady(node);
		}

		if(this.props.nodeDataChanged){
			this.props.nodeDataChanged(node);
		}


		window.$("#mainModal").modal("hide")
	}

	handleDayClick(dueDate){
		this.setState({dueDate});
	}


	render(){

		let isNew = this.props.newNodeReady!==undefined;

		let showInputEdit = true;
		let showOutputEdit = true;
		let showPercentageInput = false;
		switch(this.props.data.key){
			case "startNode":
				showInputEdit = false;
				showOutputEdit = true;
				showPercentageInput = false;
			break;
			case "dataCollectorNode":
				showInputEdit = true;
				showOutputEdit = true;
				showPercentageInput = true;
			break;
			case "checkPointNode":
				showInputEdit = false;
				showOutputEdit = true;
				showPercentageInput = true;
			break;
			case "endNode":
				showInputEdit = false;
				showOutputEdit = false;
				showPercentageInput = false;
			break;
			default:
			break;
		}

		//console.log(this.props.data.key);

		return (< >
		    <label>Node Name</label>
		   	<input autoFocus={true} name="name"
		   	onChange={(e)=>{
		   		this.setState({[e.target.name]:e.target.value});
		   	}} value={this.state.name} type="text" className="form-control" />
		   	{
		   		this.props.data.hasDueDate ? <div className="mt-2">
				   	<label>Due Date</label><br/>
				   	<DayPickerInput formatDate={formatDate}
		        	parseDate={parseDate}
		        	placeholder={`${formatDate(new Date())}`} className="form-control" onDayChange={this.handleDayClick.bind(this)}/>
				</div> : null
		   	}


		   	<div className="mt-2">
		   			<label>Description</label>
		   			<textarea onChange={(e)=>{
		   					this.setState({[e.target.name]:e.target.value});
		   			}} value={this.state.desc} className="form-control" name="desc"></textarea>
		   	</div> 

		   	{
		   		showPercentageInput ? <div className="mt-2">
				   	<label>Percentage %</label><br/>
				   	<input autoFocus={true} name="percentage" min={0} max={100}
				   	onChange={(e)=>{
				   		this.setState({[e.target.name]:e.target.value});
				   	}} value={this.state.percentage} type="number" className="form-control" />
				</div> : null
		   	}


		   	{ showInputEdit ? 
		   		<div className="mt-2">
		   			<label>Input ports (seperated by ;)</label>
		   			<textarea onChange={this.handlePortsIO.bind(this)} value={this.state.inputs} className="form-control" name="inputs"></textarea>
		   		</div> : null
		   	}

		   	{ showOutputEdit ? 
		   		<div className="mt-2">
		   			<label>Output ports (seperated by ;)</label>
		   			<textarea onChange={this.handlePortsIO.bind(this)} value={this.state.outputs} className="form-control" name="outputs"></textarea>
		   		</div> : null
		   	}
		   
		   	
		   	<div className="clear"></div>
		   	<div className="mt-2 text-center">
		   	<button onClick={this.submit.bind(this)} className="btn btn-sm btn-info mt-2">CONFIRM</button>
		   	</div>
		   	</>
		  );
		
		
	}
}

export default NodeModalForm;
