import React, { Component } from 'react';
import createEngine, { DiagramModel } from '@projectstorm/react-diagrams';
import { CanvasWidget , DeleteItemsAction} from '@projectstorm/react-canvas-core';
//import * as A from '@projectstorm/react-canvas-core';
//import gsap from 'gsap';
import _ from "lodash"
import { JSCustomNodeFactory } from '../../components/farseerNode/NodeFactory';
import { JSCustomNodeModel } from '../../components/farseerNode/NodeModel';

import NodeModalForm from "./nodeModalForm"
import { setModalContent } from "../../data/modal"
import pubsub from "../../data/pubsub"


class FarseerDiagramSample extends Component{
	constructor(){
		super();
		//1) setup the diagram engine

	/*
		var engine = createEngine();
		engine.getNodeFactories().registerFactory(new JSCustomNodeFactory());

		//2) setup the diagram model
		var model = new DiagramModel();
		engine.setModel(model);
		*/



	//5) load model into engine
	//1) setup the diagram engine
	var engine = createEngine({ registerDefaultDeleteItemsAction: false });
		engine.getNodeFactories().registerFactory(new JSCustomNodeFactory());
	//2) setup the diagram model
	var model = new DiagramModel();

	engine.setModel(model);


	//!------------- SERIALIZING ------------------

	//var str = '{ "id": "27", "offsetX": 0, "offsetY": 0, "zoom": 100, "gridSize": 0, "layers": [ { "id": "28", "type": "diagram-links", "isSvg": true, "transformed": true, "models": { "36": { "id": "36", "type": "default", "source": "32", "sourcePort": "33", "target": "34", "targetPort": "35", "points": [ { "id": "37", "type": "point", "x": 147.234375, "y": 133.5 }, { "id": "38", "type": "point", "x": 409.5, "y": 133.5 } ], "labels": [], "width": 3, "color": "gray", "curvyness": 50, "selectedColor": "rgb(0,192,255)" } } }, { "id": "30", "type": "diagram-nodes", "isSvg": false, "transformed": true, "models": { "32": { "id": "32", "type": "default", "x": 100, "y": 100, "ports": [ { "id": "33", "type": "default", "x": 139.734375, "y": 126, "name": "Out", "alignment": "right", "parentNode": "32", "links": [ "36" ], "in": false, "label": "Out" } ], "name": "Node 1", "color": "rgb(0,192,255)", "portsInOrder": [], "portsOutOrder": [ "33" ] }, "34": { "id": "34", "type": "default", "x": 400, "y": 100, "ports": [ { "id": "35", "type": "default", "x": 402, "y": 126, "name": "In", "alignment": "left", "parentNode": "34", "links": [ "36" ], "in": true, "label": "In" } ], "name": "Node 2", "color": "rgb(192,255,0)", "portsInOrder": [ "35" ], "portsOutOrder": [] } } } ] }';
	var str = '{"id":"6ada8770-2d06-4635-a5e3-29f47596a894","offsetX":32,"offsetY":20,"zoom":100,"gridSize":0,"layers":[{"id":"157bd6d5-df10-4827-8471-2ee219ad6195","type":"diagram-links","isSvg":true,"transformed":true,"models":{"438de1f5-b0c9-439e-8d46-d95a43712e11":{"id":"438de1f5-b0c9-439e-8d46-d95a43712e11","type":"default","selected":false,"source":"c1be2a08-4f8b-4360-a602-80262b19f4a0","sourcePort":"0b88d096-f6c5-4cd8-8df9-bb95ade1ac75","target":"eadcddb0-ab3b-4919-8751-3da25068ca12","targetPort":"4296494e-abd7-4f58-a6a5-b4cfc5416123","points":[{"id":"cc96e05c-7262-4069-9bf7-7616ecd437b0","type":"point","x":192.84375,"y":106},{"id":"42c1e864-69d9-436e-92de-11f26e3755e9","type":"point","selected":true,"x":248,"y":244}],"labels":[],"width":3,"color":"gray","curvyness":50,"selectedColor":"rgb(0,192,255)"},"8af8a220-320f-4596-bbea-1e22cbd1daa8":{"id":"8af8a220-320f-4596-bbea-1e22cbd1daa8","type":"default","selected":false,"source":"c1be2a08-4f8b-4360-a602-80262b19f4a0","sourcePort":"74ddb4f1-7f8e-4e76-a7f9-5942e4ec53b6","target":"eadcddb0-ab3b-4919-8751-3da25068ca12","targetPort":"4296494e-abd7-4f58-a6a5-b4cfc5416123","points":[{"id":"eb9f173b-72e9-40d9-865c-fdaa3ca9a8f7","type":"point","x":192.84375,"y":123},{"id":"33523954-5812-4caa-ab9d-64ab888ec5d4","type":"point","selected":true,"x":248,"y":244}],"labels":[],"width":3,"color":"gray","curvyness":50,"selectedColor":"rgb(0,192,255)"},"c16742f3-afc0-42ff-96b1-4058ee3ec1b9":{"id":"c16742f3-afc0-42ff-96b1-4058ee3ec1b9","type":"default","selected":false,"source":"c1be2a08-4f8b-4360-a602-80262b19f4a0","sourcePort":"90711b51-68f8-4620-a32e-7ccf46afc0b9","target":"eadcddb0-ab3b-4919-8751-3da25068ca12","targetPort":"4296494e-abd7-4f58-a6a5-b4cfc5416123","points":[{"id":"0cdead27-48c2-4bd7-bba0-fecd7a5cd718","type":"point","x":192.84375,"y":140},{"id":"8f90594d-b73c-4dbf-a6b6-4808f4ee7e44","type":"point","selected":true,"x":248,"y":244}],"labels":[],"width":3,"color":"gray","curvyness":50,"selectedColor":"rgb(0,192,255)"},"7c94cd6d-8513-4e12-aa61-322aa798b2fa":{"id":"7c94cd6d-8513-4e12-aa61-322aa798b2fa","type":"default","selected":false,"source":"b25027e8-91d2-4900-925b-2ff59e521874","sourcePort":"3c348bcc-b914-4ae1-884f-54b1383684db","target":"eadcddb0-ab3b-4919-8751-3da25068ca12","targetPort":"bde9ef38-d5a4-4f6a-b096-c685ec96e95f","points":[{"id":"b8def66f-dc10-4fed-bc03-abf6f9ad1b94","type":"point","x":78.140625,"y":226},{"id":"010fae3a-231a-499a-a168-6e0115bd49d6","type":"point","selected":true,"x":248,"y":261}],"labels":[],"width":3,"color":"gray","curvyness":50,"selectedColor":"rgb(0,192,255)"},"6be619de-f265-4390-ac33-f591c10b6956":{"id":"6be619de-f265-4390-ac33-f591c10b6956","type":"default","selected":false,"source":"b25027e8-91d2-4900-925b-2ff59e521874","sourcePort":"67601ffe-ff82-4765-8bc1-c584e0d505f1","target":"eadcddb0-ab3b-4919-8751-3da25068ca12","targetPort":"bde9ef38-d5a4-4f6a-b096-c685ec96e95f","points":[{"id":"89099e3c-7f30-4872-bc18-f284f329b074","type":"point","x":78.140625,"y":243},{"id":"20f7c966-68b3-4bf4-aee6-e32bdfaad625","type":"point","selected":true,"x":248,"y":261}],"labels":[],"width":3,"color":"gray","curvyness":50,"selectedColor":"rgb(0,192,255)"},"e9886d91-7ec7-4a30-bf42-edb9e831c6fb":{"id":"e9886d91-7ec7-4a30-bf42-edb9e831c6fb","type":"default","selected":false,"source":"b25027e8-91d2-4900-925b-2ff59e521874","sourcePort":"6e44a3fe-24f7-4061-9261-a5fc1170a8f3","target":"eadcddb0-ab3b-4919-8751-3da25068ca12","targetPort":"bde9ef38-d5a4-4f6a-b096-c685ec96e95f","points":[{"id":"a8f7ca79-8380-4e3c-810a-7c18a72adb27","type":"point","x":78.140625,"y":260},{"id":"5c2cf6b7-aed6-4df7-9f5b-a8785e01db48","type":"point","selected":true,"x":248,"y":261}],"labels":[],"width":3,"color":"gray","curvyness":50,"selectedColor":"rgb(0,192,255)"},"db379dca-77a5-42bd-9302-9bcf024d37bc":{"id":"db379dca-77a5-42bd-9302-9bcf024d37bc","type":"default","selected":false,"source":"7983e07f-fcb8-4299-9f48-255d4951f7bc","sourcePort":"3cf24671-765e-4b0e-b1c9-3b9294feb55a","target":"eadcddb0-ab3b-4919-8751-3da25068ca12","targetPort":"bde9ef38-d5a4-4f6a-b096-c685ec96e95f","points":[{"id":"2a0ea582-a3d7-4cfb-a140-66822b85b406","type":"point","x":139.109375,"y":405},{"id":"1969249e-2431-4b62-a49f-e6984488c110","type":"point","selected":true,"x":248,"y":261}],"labels":[],"width":3,"color":"gray","curvyness":50,"selectedColor":"rgb(0,192,255)"},"eaeab1d7-9b40-490b-a4a4-4fa2185cea40":{"id":"eaeab1d7-9b40-490b-a4a4-4fa2185cea40","type":"default","selected":false,"source":"eadcddb0-ab3b-4919-8751-3da25068ca12","sourcePort":"d41bbf57-f4da-469e-9726-6b6120377b7f","target":"a87f0d7b-79bc-40db-986e-65647faddea1","targetPort":"92d03ac4-c081-4c39-9ed4-f7cf421e40a7","points":[{"id":"3ff60f7b-a1d0-4f22-826b-4c2a42f1134e","type":"point","x":393.234375,"y":244},{"id":"da56b6e2-ab2e-4599-a592-bf0acb4cb2bb","type":"point","x":469,"y":235}],"labels":[],"width":3,"color":"gray","curvyness":50,"selectedColor":"rgb(0,192,255)"},"a80a6164-15df-43a2-b1b4-ba5dd14f9a38":{"id":"a80a6164-15df-43a2-b1b4-ba5dd14f9a38","type":"default","selected":false,"source":"a87f0d7b-79bc-40db-986e-65647faddea1","sourcePort":"652e1f8b-4837-4558-ad9e-2a70df7e5923","target":"a7e7bd53-27a3-42da-acfc-c7d412544982","targetPort":"8f42337b-c82e-4881-97b0-bfd7f2b58bc5","points":[{"id":"e764f49f-1be9-45e7-a32f-91c7f4f08e25","type":"point","x":627.28125,"y":235},{"id":"90a7941c-d293-4971-9b0b-f658608b3bde","type":"point","x":788,"y":314}],"labels":[],"width":3,"color":"gray","curvyness":50,"selectedColor":"rgb(0,192,255)"},"49470ce2-4e30-4336-aa38-1e4ad1b14e22":{"id":"49470ce2-4e30-4336-aa38-1e4ad1b14e22","type":"default","selected":false,"source":"a87f0d7b-79bc-40db-986e-65647faddea1","sourcePort":"cfac7dbf-f250-467a-9558-3f6e5938c56a","target":"7c0bbb82-d54d-4524-9e1e-6ff9b8da3f12","targetPort":"a83a0c6a-f629-48fa-8b82-997d5c07c5e4","points":[{"id":"c0b9e118-cbdd-4af4-8ded-d5876d9bab9a","type":"point","x":627.28125,"y":252},{"id":"bf66be9d-4c4a-48f0-ae5e-8386003ed7c8","type":"point","selected":false,"x":625.84375,"y":346},{"id":"cceffc56-e3e8-4d3b-a8af-06ad934b6046","type":"point","selected":false,"x":231.84375,"y":343},{"id":"08ff3a5a-6dac-4dbe-b4ef-09ca44d2cd6e","type":"point","selected":false,"x":228.84375,"y":520},{"id":"a846be96-ea1e-48eb-9766-ef5c01548414","type":"point","x":271,"y":520}],"labels":[],"width":3,"color":"gray","curvyness":50,"selectedColor":"rgb(0,192,255)"},"93f24b77-c2e4-463d-a5c4-ed2d3b28f99a":{"id":"93f24b77-c2e4-463d-a5c4-ed2d3b28f99a","type":"default","selected":false,"source":"7c0bbb82-d54d-4524-9e1e-6ff9b8da3f12","sourcePort":"f3f396a0-b753-4fbe-9750-4495247daa3f","target":"83b06b03-1118-4491-98c4-cb4b6a555a5d","targetPort":"eb949aff-526c-485f-9f6d-7c842d87291d","points":[{"id":"b4229092-e48f-45c8-990d-6161abf35e3f","type":"point","x":438.0625,"y":520},{"id":"a2af1d54-3829-4d20-b7ac-f18cb6ff0925","type":"point","x":525,"y":550}],"labels":[],"width":3,"color":"gray","curvyness":50,"selectedColor":"rgb(0,192,255)"},"095eb84e-0e82-4cc2-8388-e63954bd53be":{"id":"095eb84e-0e82-4cc2-8388-e63954bd53be","type":"default","selected":false,"source":"7c0bbb82-d54d-4524-9e1e-6ff9b8da3f12","sourcePort":"9d8a44f2-b1bd-41c2-a05e-84b3efe79406","target":"83b06b03-1118-4491-98c4-cb4b6a555a5d","targetPort":"eb949aff-526c-485f-9f6d-7c842d87291d","points":[{"id":"c06c9b0e-a430-4f1a-b5f3-ad0c619de879","type":"point","x":438.0625,"y":537},{"id":"9f04ad90-55e7-4997-80af-455e50239958","type":"point","x":525,"y":550}],"labels":[],"width":3,"color":"gray","curvyness":50,"selectedColor":"rgb(0,192,255)"},"8dd01e96-7444-4972-8c3b-48611b1a3e01":{"id":"8dd01e96-7444-4972-8c3b-48611b1a3e01","type":"default","selected":false,"source":"7c0bbb82-d54d-4524-9e1e-6ff9b8da3f12","sourcePort":"3f85cd56-46f9-40f4-8455-ec8552e33d76","target":"83b06b03-1118-4491-98c4-cb4b6a555a5d","targetPort":"eb949aff-526c-485f-9f6d-7c842d87291d","points":[{"id":"0bfb6e20-e9ee-45a3-bd77-f2f04fc45104","type":"point","x":438.0625,"y":554},{"id":"c75b87a3-6815-4735-8071-333336c4b1ae","type":"point","x":525,"y":550}],"labels":[],"width":3,"color":"gray","curvyness":50,"selectedColor":"rgb(0,192,255)"},"c093b34d-3788-426f-9636-55f38af5d9d4":{"id":"c093b34d-3788-426f-9636-55f38af5d9d4","type":"default","selected":false,"source":"7c0bbb82-d54d-4524-9e1e-6ff9b8da3f12","sourcePort":"c164e7bf-a1d5-483f-b254-c0f02653c760","target":"83b06b03-1118-4491-98c4-cb4b6a555a5d","targetPort":"eb949aff-526c-485f-9f6d-7c842d87291d","points":[{"id":"aab52f6d-e66b-4808-8276-3aee59168c45","type":"point","x":438.0625,"y":571},{"id":"4d1ca0cd-1b8a-40e6-b014-dff4e2527d83","type":"point","x":525,"y":550}],"labels":[],"width":3,"color":"gray","curvyness":50,"selectedColor":"rgb(0,192,255)"},"c839c887-6542-4db9-a9a7-64c4ee0c031f":{"id":"c839c887-6542-4db9-a9a7-64c4ee0c031f","type":"default","selected":false,"source":"7c0bbb82-d54d-4524-9e1e-6ff9b8da3f12","sourcePort":"711201d9-9eab-4955-a682-f1c2459132d7","target":"83b06b03-1118-4491-98c4-cb4b6a555a5d","targetPort":"eb949aff-526c-485f-9f6d-7c842d87291d","points":[{"id":"3af186fa-80b4-401f-8001-6af54df8456d","type":"point","x":438.0625,"y":588},{"id":"29e9171e-ee4f-45a8-b9fc-8111d8d28cd5","type":"point","x":525,"y":550}],"labels":[],"width":3,"color":"gray","curvyness":50,"selectedColor":"rgb(0,192,255)"},"eb3c5e84-bcc8-4351-8246-64ce06480d3b":{"id":"eb3c5e84-bcc8-4351-8246-64ce06480d3b","type":"default","selected":false,"source":"83b06b03-1118-4491-98c4-cb4b6a555a5d","sourcePort":"66d22266-6ea4-4f51-809b-d5d71d07126d","target":"a7e7bd53-27a3-42da-acfc-c7d412544982","targetPort":"8f42337b-c82e-4881-97b0-bfd7f2b58bc5","points":[{"id":"c5c5e1a0-a2b8-416c-b74b-b3b23c25ddeb","type":"point","x":683.28125,"y":550},{"id":"eb225f14-e801-40a1-a379-9d7f96acbb81","type":"point","x":788,"y":314}],"labels":[],"width":3,"color":"gray","curvyness":50,"selectedColor":"rgb(0,192,255)"},"790fc30a-ef22-4b9e-b3ca-e9c568c25309":{"id":"790fc30a-ef22-4b9e-b3ca-e9c568c25309","type":"default","selected":false,"source":"83b06b03-1118-4491-98c4-cb4b6a555a5d","sourcePort":"14869b3b-b715-4628-bcd4-e9e4aefee726","target":"bf3aaee5-d50b-4202-ae3d-c695bba38c37","targetPort":"c088fa37-b316-4ff7-9095-282b8a04a9e5","points":[{"id":"bec41884-89b2-48e1-bb81-5547d4e60581","type":"point","x":683.28125,"y":567},{"id":"755e107f-b2aa-4581-aa30-a5802a97aa2e","type":"point","x":754,"y":590}],"labels":[],"width":3,"color":"gray","curvyness":50,"selectedColor":"rgb(0,192,255)"},"4e32e3a5-e10a-4fa6-bef5-de4db079c18b":{"id":"4e32e3a5-e10a-4fa6-bef5-de4db079c18b","type":"default","selected":false,"source":"bf3aaee5-d50b-4202-ae3d-c695bba38c37","sourcePort":"1a8c83f9-c332-48ce-8490-45995bae4f63","target":"9cabd79b-9f9f-42e5-8e5c-223b6c4f0a4a","targetPort":"f97028bb-80a0-4400-9387-f5ca9a7bdb84","points":[{"id":"fc3d2179-cf54-48d5-8b6e-0794cccb6c1f","type":"point","x":873.1875,"y":590},{"id":"1732042c-686e-409a-bd14-d8e8c54ef293","type":"point","x":946,"y":531}],"labels":[],"width":3,"color":"gray","curvyness":50,"selectedColor":"rgb(0,192,255)"}}},{"id":"55b6091e-35aa-4b1c-9c8f-956e90273a51","type":"diagram-nodes","isSvg":false,"transformed":true,"models":{"c1be2a08-4f8b-4360-a602-80262b19f4a0":{"id":"c1be2a08-4f8b-4360-a602-80262b19f4a0","type":"farseer-node","selected":false,"x":70,"y":38,"ports":[{"id":"0b88d096-f6c5-4cd8-8df9-bb95ade1ac75","type":"default","x":184.84375,"y":98,"name":"TARIFA 1","alignment":"right","parentNode":"c1be2a08-4f8b-4360-a602-80262b19f4a0","links":["438de1f5-b0c9-439e-8d46-d95a43712e11"],"in":false,"label":"TARIFA 1"},{"id":"74ddb4f1-7f8e-4e76-a7f9-5942e4ec53b6","type":"default","x":184.84375,"y":115,"name":" TARIFA 2","alignment":"right","parentNode":"c1be2a08-4f8b-4360-a602-80262b19f4a0","links":["8af8a220-320f-4596-bbea-1e22cbd1daa8"],"in":false,"label":" TARIFA 2"},{"id":"90711b51-68f8-4620-a32e-7ccf46afc0b9","type":"default","x":184.84375,"y":132,"name":" TARIFA 3","alignment":"right","parentNode":"c1be2a08-4f8b-4360-a602-80262b19f4a0","links":["c16742f3-afc0-42ff-96b1-4058ee3ec1b9"],"in":false,"label":" TARIFA 3"}],"fsData":{"key":"startNode","name":"TRANSPORNI SUSTAVI","desc":"Nove tarife","backgroundColor":"rgb(0, 192, 255)","icon":"fa fa-flag-checkered","percentage":100,"confirmed":false,"hasDueDate":false,"inputs":[],"outputs":["TARIFA 1"," TARIFA 2"," TARIFA 3"],"dueDate":"2020-03-03T10:39:01.106Z"}},"b25027e8-91d2-4900-925b-2ff59e521874":{"id":"b25027e8-91d2-4900-925b-2ff59e521874","type":"farseer-node","selected":false,"x":-1,"y":158,"ports":[{"id":"3c348bcc-b914-4ae1-884f-54b1383684db","type":"default","x":70.140625,"y":218,"name":"REGIJA A","alignment":"right","parentNode":"b25027e8-91d2-4900-925b-2ff59e521874","links":["7c94cd6d-8513-4e12-aa61-322aa798b2fa"],"in":false,"label":"REGIJA A"},{"id":"67601ffe-ff82-4765-8bc1-c584e0d505f1","type":"default","x":70.140625,"y":235,"name":" REGIJA B","alignment":"right","parentNode":"b25027e8-91d2-4900-925b-2ff59e521874","links":["6be619de-f265-4390-ac33-f591c10b6956"],"in":false,"label":" REGIJA B"},{"id":"6e44a3fe-24f7-4061-9261-a5fc1170a8f3","type":"default","x":70.140625,"y":252,"name":" REGIJA C","alignment":"right","parentNode":"b25027e8-91d2-4900-925b-2ff59e521874","links":["e9886d91-7ec7-4a30-bf42-edb9e831c6fb"],"in":false,"label":" REGIJA C"}],"fsData":{"key":"startNode","name":"PRODAJA","desc":"PRIHODI","backgroundColor":"rgb(0, 192, 255)","icon":"fa fa-flag-checkered","percentage":100,"confirmed":false,"hasDueDate":false,"inputs":[],"outputs":["REGIJA A"," REGIJA B"," REGIJA C"],"dueDate":"2020-03-03T10:39:01.106Z"}},"eadcddb0-ab3b-4919-8751-3da25068ca12":{"id":"eadcddb0-ab3b-4919-8751-3da25068ca12","type":"farseer-node","selected":false,"x":239,"y":149,"ports":[{"id":"d41bbf57-f4da-469e-9726-6b6120377b7f","type":"default","x":385.234375,"y":236,"name":"DATA OUT","alignment":"right","parentNode":"eadcddb0-ab3b-4919-8751-3da25068ca12","links":["eaeab1d7-9b40-490b-a4a4-4fa2185cea40"],"in":false,"label":"DATA OUT"},{"id":"4296494e-abd7-4f58-a6a5-b4cfc5416123","type":"default","x":240,"y":236,"name":"TARIFE","alignment":"left","parentNode":"eadcddb0-ab3b-4919-8751-3da25068ca12","links":["438de1f5-b0c9-439e-8d46-d95a43712e11","8af8a220-320f-4596-bbea-1e22cbd1daa8","c16742f3-afc0-42ff-96b1-4058ee3ec1b9"],"in":true,"label":"TARIFE"},{"id":"bde9ef38-d5a4-4f6a-b096-c685ec96e95f","type":"default","x":240,"y":253,"name":" PRIHODI","alignment":"left","parentNode":"eadcddb0-ab3b-4919-8751-3da25068ca12","links":["7c94cd6d-8513-4e12-aa61-322aa798b2fa","6be619de-f265-4390-ac33-f591c10b6956","e9886d91-7ec7-4a30-bf42-edb9e831c6fb","db379dca-77a5-42bd-9302-9bcf024d37bc"],"in":true,"label":" PRIHODI"}],"fsData":{"key":"dataCollectorNode","name":"OBRAZAC ZA PLANIRANJE","desc":"Sukladno radionici","backgroundColor":"#16DB93","icon":"fa fa-filter","percentage":10,"confirmed":false,"hasDueDate":true,"inputs":["TARIFE"," PRIHODI"],"outputs":["DATA OUT"],"dueDate":"2020-03-11T11:00:00.000Z"}},"7983e07f-fcb8-4299-9f48-255d4951f7bc":{"id":"7983e07f-fcb8-4299-9f48-255d4951f7bc","type":"farseer-node","selected":false,"x":45,"y":337,"ports":[{"id":"3cf24671-765e-4b0e-b1c9-3b9294feb55a","type":"default","x":131.109375,"y":397,"name":"PRIHOD+3%","alignment":"right","parentNode":"7983e07f-fcb8-4299-9f48-255d4951f7bc","links":["db379dca-77a5-42bd-9302-9bcf024d37bc"],"in":false,"label":"PRIHOD+3%"}],"fsData":{"key":"startNode","name":"STRATEGIJE","desc":"Očekivanja NO","backgroundColor":"rgb(0, 192, 255)","icon":"fa fa-flag-checkered","percentage":100,"confirmed":false,"hasDueDate":false,"inputs":[],"outputs":["PRIHOD+3%"]}},"a87f0d7b-79bc-40db-986e-65647faddea1":{"id":"a87f0d7b-79bc-40db-986e-65647faddea1","type":"farseer-node","selected":false,"x":460,"y":156,"ports":[{"id":"92d03ac4-c081-4c39-9ed4-f7cf421e40a7","type":"default","x":461,"y":227,"name":"INPUTS","alignment":"left","parentNode":"a87f0d7b-79bc-40db-986e-65647faddea1","links":["eaeab1d7-9b40-490b-a4a4-4fa2185cea40"],"in":true,"label":"INPUTS"},{"id":"652e1f8b-4837-4558-ad9e-2a70df7e5923","type":"default","x":619.28125,"y":227,"name":"ODBIJENO","alignment":"right","parentNode":"a87f0d7b-79bc-40db-986e-65647faddea1","links":["a80a6164-15df-43a2-b1b4-ba5dd14f9a38"],"in":false,"label":"ODBIJENO"},{"id":"cfac7dbf-f250-467a-9558-3f6e5938c56a","type":"default","x":619.28125,"y":244,"name":"PRIHVAĆENO","alignment":"right","parentNode":"a87f0d7b-79bc-40db-986e-65647faddea1","links":["49470ce2-4e30-4336-aa38-1e4ad1b14e22"],"in":false,"label":"PRIHVAĆENO"}],"fsData":{"key":"checkPointNode","name":"DIREKTORI SEKTORA","desc":"","backgroundColor":"#FF9505","icon":"fa fa-hourglass-2","percentage":0,"confirmed":false,"hasDueDate":true,"inputs":["INPUTS"],"outputs":["ODBIJENO","PRIHVAĆENO"],"dueDate":"2020-03-13T11:00:00.000Z"}},"a7e7bd53-27a3-42da-acfc-c7d412544982":{"id":"a7e7bd53-27a3-42da-acfc-c7d412544982","type":"farseer-node","selected":false,"x":779,"y":246,"ports":[{"id":"8f42337b-c82e-4881-97b0-bfd7f2b58bc5","type":"default","x":780,"y":306,"name":"FINISH","alignment":"left","parentNode":"a7e7bd53-27a3-42da-acfc-c7d412544982","links":["a80a6164-15df-43a2-b1b4-ba5dd14f9a38","eb3c5e84-bcc8-4351-8246-64ce06480d3b"],"in":true,"label":"FINISH"}],"fsData":{"key":"endNode","name":"PONAVLJANJE PROCESA","desc":"Maskimalno 3 dana","backgroundColor":"#F8333C","icon":"fa fa-handshake-o","percentage":0,"confirmed":false,"hasDueDate":true,"inputs":["FINISH"],"outputs":[],"dueDate":"2020-03-14T11:00:00.000Z"}},"7c0bbb82-d54d-4524-9e1e-6ff9b8da3f12":{"id":"7c0bbb82-d54d-4524-9e1e-6ff9b8da3f12","type":"farseer-node","selected":false,"x":262,"y":441,"ports":[{"id":"a83a0c6a-f629-48fa-8b82-997d5c07c5e4","type":"default","x":263,"y":512,"name":"ULAZI","alignment":"left","parentNode":"7c0bbb82-d54d-4524-9e1e-6ff9b8da3f12","links":["49470ce2-4e30-4336-aa38-1e4ad1b14e22"],"in":true,"label":"ULAZI"},{"id":"f3f396a0-b753-4fbe-9750-4495247daa3f","type":"default","x":430.0625,"y":512,"name":"RDG","alignment":"right","parentNode":"7c0bbb82-d54d-4524-9e1e-6ff9b8da3f12","links":["93f24b77-c2e4-463d-a5c4-ed2d3b28f99a"],"in":false,"label":"RDG"},{"id":"9d8a44f2-b1bd-41c2-a05e-84b3efe79406","type":"default","x":430.0625,"y":529,"name":"PLAN ULAGANJA","alignment":"right","parentNode":"7c0bbb82-d54d-4524-9e1e-6ff9b8da3f12","links":["095eb84e-0e82-4cc2-8388-e63954bd53be"],"in":false,"label":"PLAN ULAGANJA"},{"id":"3f85cd56-46f9-40f4-8455-ec8552e33d76","type":"default","x":430.0625,"y":546,"name":"PLAN UGOVORA","alignment":"right","parentNode":"7c0bbb82-d54d-4524-9e1e-6ff9b8da3f12","links":["8dd01e96-7444-4972-8c3b-48611b1a3e01"],"in":false,"label":"PLAN UGOVORA"},{"id":"c164e7bf-a1d5-483f-b254-c0f02653c760","type":"default","x":430.0625,"y":563,"name":"PLAN NABAVE","alignment":"right","parentNode":"7c0bbb82-d54d-4524-9e1e-6ff9b8da3f12","links":["c093b34d-3788-426f-9636-55f38af5d9d4"],"in":false,"label":"PLAN NABAVE"},{"id":"711201d9-9eab-4955-a682-f1c2459132d7","type":"default","x":430.0625,"y":580,"name":"PLAN KADROVA","alignment":"right","parentNode":"7c0bbb82-d54d-4524-9e1e-6ff9b8da3f12","links":["c839c887-6542-4db9-a9a7-64c4ee0c031f"],"in":false,"label":"PLAN KADROVA"}],"fsData":{"key":"dataCollectorNode","name":"Služba finaancija","desc":"","backgroundColor":"#16DB93","icon":"fa fa-filter","percentage":0,"confirmed":false,"hasDueDate":true,"inputs":["ULAZI"],"outputs":["RDG","PLAN ULAGANJA","PLAN UGOVORA","PLAN NABAVE","PLAN KADROVA"]}},"83b06b03-1118-4491-98c4-cb4b6a555a5d":{"id":"83b06b03-1118-4491-98c4-cb4b6a555a5d","type":"farseer-node","selected":false,"x":516,"y":471,"ports":[{"id":"eb949aff-526c-485f-9f6d-7c842d87291d","type":"default","x":517,"y":542,"name":"INPUTS","alignment":"left","parentNode":"83b06b03-1118-4491-98c4-cb4b6a555a5d","links":["93f24b77-c2e4-463d-a5c4-ed2d3b28f99a","095eb84e-0e82-4cc2-8388-e63954bd53be","8dd01e96-7444-4972-8c3b-48611b1a3e01","c093b34d-3788-426f-9636-55f38af5d9d4","c839c887-6542-4db9-a9a7-64c4ee0c031f"],"in":true,"label":"INPUTS"},{"id":"66d22266-6ea4-4f51-809b-d5d71d07126d","type":"default","x":675.28125,"y":542,"name":"ODBIJENO","alignment":"right","parentNode":"83b06b03-1118-4491-98c4-cb4b6a555a5d","links":["eb3c5e84-bcc8-4351-8246-64ce06480d3b"],"in":false,"label":"ODBIJENO"},{"id":"14869b3b-b715-4628-bcd4-e9e4aefee726","type":"default","x":675.28125,"y":559,"name":" PRIHVAĆENO","alignment":"right","parentNode":"83b06b03-1118-4491-98c4-cb4b6a555a5d","links":["790fc30a-ef22-4b9e-b3ca-e9c568c25309"],"in":false,"label":" PRIHVAĆENO"}],"fsData":{"key":"checkPointNode","name":"KONTROLA UPRAVE","desc":"","backgroundColor":"#FF9505","icon":"fa fa-hourglass-2","percentage":0,"confirmed":false,"hasDueDate":true,"inputs":["INPUTS"],"outputs":["ODBIJENO"," PRIHVAĆENO"],"dueDate":"2020-03-18T11:00:00.000Z"}},"bf3aaee5-d50b-4202-ae3d-c695bba38c37":{"id":"bf3aaee5-d50b-4202-ae3d-c695bba38c37","type":"farseer-node","selected":false,"x":745,"y":511,"ports":[{"id":"1a8c83f9-c332-48ce-8490-45995bae4f63","type":"default","x":865.1875,"y":582,"name":"PLAN","alignment":"right","parentNode":"bf3aaee5-d50b-4202-ae3d-c695bba38c37","links":["4e32e3a5-e10a-4fa6-bef5-de4db079c18b"],"in":false,"label":"PLAN"},{"id":"c088fa37-b316-4ff7-9095-282b8a04a9e5","type":"default","x":746,"y":582,"name":"ULAZI","alignment":"left","parentNode":"bf3aaee5-d50b-4202-ae3d-c695bba38c37","links":["790fc30a-ef22-4b9e-b3ca-e9c568c25309"],"in":true,"label":"ULAZI"}],"fsData":{"key":"dataCollectorNode","name":"FINANCIJE","desc":"","backgroundColor":"#16DB93","icon":"fa fa-filter","percentage":0,"confirmed":false,"hasDueDate":true,"inputs":["ULAZI"],"outputs":["PLAN"]}},"9cabd79b-9f9f-42e5-8e5c-223b6c4f0a4a":{"id":"9cabd79b-9f9f-42e5-8e5c-223b6c4f0a4a","type":"farseer-node","selected":false,"x":937,"y":463,"ports":[{"id":"f97028bb-80a0-4400-9387-f5ca9a7bdb84","type":"default","x":938,"y":523,"name":"FINISH","alignment":"left","parentNode":"9cabd79b-9f9f-42e5-8e5c-223b6c4f0a4a","links":["4e32e3a5-e10a-4fa6-bef5-de4db079c18b"],"in":true,"label":"FINISH"}],"fsData":{"key":"endNode","name":"IMPLEMENTACIJA","desc":"Spuštanje ciljeva","backgroundColor":"#F8333C","icon":"fa fa-handshake-o","percentage":0,"confirmed":false,"hasDueDate":true,"inputs":["FINISH"],"outputs":[]}}}}]}'
		//JSON.stringify(model.serialize());

	//!------------- DESERIALIZING ----------------

	var model2 = new DiagramModel();
	model2.deserializeModel(JSON.parse(str), engine);
	engine.setModel(model2);

	//console.log(A);
	// register an DeleteItemsAction with custom keyCodes (in this case, only Delete key)
	engine.getActionEventBus().registerAction(new DeleteItemsAction({ keyCodes: [46] }));



		//6) render the diagram!
		this.state = {
			engine: engine
		}


	}

	componentDidMount(){
		pubsub.on("nodeDoubleClick", nodeClone=>{
			let data = nodeClone.fsData
			setModalContent(<NodeModalForm id={nodeClone.options.id} nodeDataChanged={this.nodeDataChanged.bind(this, nodeClone.options.id)} data={data} />, "Edit node", false);
	    	window.$("#mainModal").modal("show");
		})

		pubsub.on("serializeGraph", ()=>{
			console.log(JSON.stringify(this.state.engine.model.serialize()));
			alert("in console");
		})
	}

	nodeDataChanged(nodeId, data){

		let node = _.find(this.state.engine.model.getNodes(), n=>n.options.id===nodeId)
		if(node){
			node.fsData = data;
			//console.log(node);
			//prvo izbriši
			_.forEach(node.getInPorts(), p=>{
				if(!_.includes(data.inputs, p.options.name)){
					node.removePort(p);
				}
			})

			_.forEach(node.getOutPorts(), p=>{
				if(!_.includes(data.outputs, p.options.name)){

					node.removePort(p);
				}
			})
			//onda rekreiraj nove portove ili izmjenjene
			_.forEach(data.inputs, name=>{
				if(!node.checkInPort(name)){
					node.addInPort(name);
				}
			})

			_.forEach(data.outputs, name=>{
				if(!node.checkOutPort(name)){
					node.addOutPort(name);
				}
			})

			this.state.engine.repaintCanvas();
		}
		

		
	}

	nodeDataReady(event, data){

		var node = new JSCustomNodeModel(data, data);

		_.forEach(data.inputs, name=>{
			node.addInPort(name);
		})

		_.forEach(data.outputs, name=>{
			node.addOutPort(name);
		})

		var point = this.state.engine.getRelativeMousePoint(event);
		node.setPosition(point);
		this.state.engine
			.getModel()
			.addNode(node);
		this.forceUpdate();
	}

	render(){
		return (<div onDragOver={(event) => {event.preventDefault();}}
			onDrop={(event)=>{
	    	var data = JSON.parse(event.dataTransfer.getData('storm-diagram-node'));
	    	event.persist();
	    	setModalContent(<NodeModalForm newNodeReady={this.nodeDataReady.bind(this, event)} data={data}/>, "Add new: "+data.name, false);
	    	window.$("#mainModal").modal("show");
				
	    }}>

	 	    
	    <CanvasWidget className="diagram-container" engine={this.state.engine} />
	    
	    </div>)
	}
}

export default FarseerDiagramSample;
