import React from 'react';
import FarseerNodeTypes from "../../components/farseerNode/nodeModelTypes"
import _ from "lodash"
import pubsub from "../../data/pubsub"

function NodeTemplate(props) {
	return (<div draggable={true} 
		onDragStart={event => {
			let model = _.clone(props.model);
			event.dataTransfer.setData('storm-diagram-node', JSON.stringify(model));
		}}
	 	className="my-node " style={{backgroundColor:props.model.backgroundColor}}>
			<div className="header"><b>{props.model.name}</b></div>
			<div className="body"> <i className={" fa-2x p-2 "+ props.model.icon}/> {props.model.desc}</div>
		</div>);
}



function SideToolBox() {
	return (<div>
		{
			_.map(FarseerNodeTypes, node=>{
				return <NodeTemplate key={node.key} model={node}/>
			})
		}

		<button onClick={()=>{
			pubsub.emit("serializeGraph")
		}} className="btn btn-info mt-2">SERIALIZE</button>
		</div>);
}


export default SideToolBox;