import React from 'react';
import { HashRouter as Router, Switch, Route, Redirect } from "react-router-dom"
import { withRouter } from 'react-router'

import MasterLayout from "./pages/masterLayout"
import Sample1 from "./pages/sample1"

var MasterLayoutWithRouter = withRouter(MasterLayout)

const PublicRoutes = (props) => {
	function onEnterRouteHandler(pageTitle, Component, nextRouteObj){
		//baobabTree.root.select("pageTitle").set(pageTitle);
		return <Component/>;
	}
	return (
	<Router {...props}  >
		<MasterLayoutWithRouter {...props}  >
		<Switch {...props}>  
			<Route exact path="/" children={onEnterRouteHandler.bind(this, "Sample 1", withRouter(Sample1))} />

			<Route title="Page not found" children={()=>{return <Redirect to="/" />}} />
		</Switch>

		</MasterLayoutWithRouter>
	</Router>
);
}


export default PublicRoutes;

