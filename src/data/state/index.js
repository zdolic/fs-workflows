import Baobab from "baobab";


var initialStateObject = ({
  model:{
    
  },
  locale:"hr",
  modalData: {
    width: "", //modal-sm || modal-lg || modal-xl
    header: "Modal window title",
    content: null, //it may be an react class instance
    showBottomCloseButton: false,
    params: {}, //modal params used optionaly by modal specific modal internals
    bodyStyle:{}
  },
  authTokenInfo: null, //it is an object containing token and user informations. it is obtained on logon!
});


var cachedStateObject = null;

try {
  cachedStateObject = window.localStorage.baobab !== undefined ?
    JSON.parse(window.localStorage.baobab) : initialStateObject;
} catch (err) {
  console.warn(err)
}


initialStateObject.authTokenInfo = cachedStateObject.authTokenInfo;
initialStateObject.locale = cachedStateObject.locale;

var tree = new Baobab(initialStateObject, { immutable: true });


const saveCachedBaobabData = (baobabTree) => {
  if (typeof (Storage) !== "undefined") {
    if (baobabTree._data) {
      window.localStorage.baobab = JSON.stringify(baobabTree._data);
    } else {
      window.localStorage.baobab = JSON.stringify(baobabTree);
    }
  } else {
    //console.log("Sorry no WEB storage support.");
    // Sorry! No Web Storage support..
  }
}

export default tree;

export {saveCachedBaobabData, tree}

