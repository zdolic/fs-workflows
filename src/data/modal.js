
import baobabTree from "./state"
//import _ from "lodash"
/*
modalData:{
      width: "", //modal-sm || modal-lg || modal-xl
      header:"Modal window title",
      content:null, //it may be an react class instance
      showBottomCloseButton: false
    },
*/


const setModalContent =(content, header, showBottomCloseButton = true, width = undefined, modalParams={}, bodyStyle={})=>{	
	baobabTree.root.select("modalData").set({
		content,
		header,
		showBottomCloseButton,
		width,
		params:modalParams,
		bodyStyle
	});
}


const setModalContentObject =(content, cfg)=>{	
	//header, showBottomCloseButton = true, width = undefined, modalParams={}
	baobabTree.root.select("modalData").set({
		content,
		header:cfg.header ? cfg.header : "",
		showBottomCloseButton: cfg.showBottomCloseButton!== undefined ? cfg.showBottomCloseButton : true,
		width: cfg.width ? cfg.width : "",
		params:cfg.modalParams ? cfg.modalParams : {}
	});
}

export {setModalContent, setModalContentObject}