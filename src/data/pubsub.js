
const EventEmitter = require('events');
class addOnEventEmitter extends EventEmitter {}

var eventEmitter = null;
if(window.pimatico_hotel_pubsub){
	eventEmitter = window.pimatico_hotel_pubsub;
} else {
	eventEmitter = new addOnEventEmitter();
	 window.pimatico_hotel_pubsub = eventEmitter;
}

export default eventEmitter;